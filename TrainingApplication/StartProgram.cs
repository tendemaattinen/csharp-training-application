﻿/*
 * Author: Teemu Tynkkynen
 * Description: Starting page of the program. User can log in or register a new account.
 * 
 */

using System;
using System.Data.SQLite;
using System.IO;

namespace TrainingApplication
{
    class StartProgram
    {
        static void Main(string[] args)
        {
            SQLiteConnection connection = null;
            bool loop = false;
            bool loggedIn = false;
            User user;

            // Initialize connection to account database. If failed, closes program.
            try
            {
                connection = InitializeApplication("accountDatabase.sqlite", (canLogin) => loop = canLogin);
                connection.Open();

                Console.WriteLine("*** Welcome ***");

                while (loop)
                {
                    user = ChooseAction(PrintLoginMenu(), connection, (isUserLoggedIn) => loggedIn = isUserLoggedIn, (continueLoop) => loop = continueLoop);

                    if (loggedIn)
                    {
                        var mainProgram = new MainProgram();
                        mainProgram.Main(user, connection);
                    }

                    loggedIn = false;
                }
                connection.Close();

                // Testing code!!

                // Applications.ApplicationsMenu();

                // !!! Testing code ends !!! 

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static SQLiteConnection InitializeApplication(string databaseName, Action<bool> isLoginSuccessful)
        {
            SQLiteConnection conn = null;
            SQLiteCommand command;
            string sql;
            var path = "Databases/" + databaseName;

            try
            {
                if (!File.Exists(path))
                {
                    SQLiteConnection.CreateFile(path);
                    conn = new SQLiteConnection("Data Source=" + path + ";Version=3;");
                    conn.Open();

                    sql = "CREATE TABLE accounts (uid INTEGER PRIMARY KEY, username VARCHAR(50), password VARCHAR(50), isadmin INTEGER)";
                    command = new SQLiteCommand(sql, conn);
                    command.ExecuteNonQuery();

                    var adminPasswordAsHash = Hash.GenerateHash("root");

                    sql = "INSERT INTO accounts (username, password, isadmin) VALUES (@un, @pw, @isadmin)";
                    command = new SQLiteCommand(sql, conn);
                    command.Parameters.AddWithValue("@un", "admin");
                    command.Parameters.AddWithValue("@pw", adminPasswordAsHash);
                    command.Parameters.AddWithValue("@isadmin", 1);
                    command.ExecuteNonQuery();

                    conn.Close();
                }
                else
                {
                    conn = new SQLiteConnection("Data Source=" + path + ";Version=3;");
                }
                isLoginSuccessful(true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                isLoginSuccessful(false);
            }
            return conn;
        }

        private static string PrintLoginMenu()
        {
            Console.WriteLine();
            Console.WriteLine("1) Log in");
            Console.WriteLine("2) Register a new account");
            Console.WriteLine("3) Info");
            Console.WriteLine("0) Quit");
            Console.Write("Enter: ");
            return Console.ReadLine().ToLower();
        }

        private static User ChooseAction(string input, SQLiteConnection conn, Action<bool> isUserLoggedIn, Action<bool> continueLoop)
        {
            continueLoop(true);
            bool logged = false;
            isUserLoggedIn(logged);
            User user = new User();
            switch (input)
            {
                case "1":
                    user = LogIn.TryToLoginIn(conn, (isLoginSuccessful) => logged = isLoginSuccessful);
                    isUserLoggedIn(logged);
                    break;
                case "2":
                    Register.RegisterNewAccount(conn);
                    break;
                case "3":
                case "help":
                case "info":
                    TextFileReader.ReadTextFile("infoHelpStart", "infoHelpEnd");
                    break;
                case "0":
                case "quit":
                case "q":
                case "exit":
                    continueLoop(false);
                    break;
            }
            return user;
        }
    }
}