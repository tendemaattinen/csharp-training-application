﻿/*
 * Author: Teemu Tynkkynen
 * Description: Class that handles directing to right applications.
 * 
 */

using System;

namespace TrainingApplication
{
    class Applications
    {
        public static void ApplicationsMenu()
        {
            var loop = true;
            Console.WriteLine();
            Console.Write("*** Applications ***");
            Console.WriteLine();
            while (loop)
            {
                loop = ChooseAction(PrintApplicationsMenu());
            }
        }

        private static bool ChooseAction(string input)
        {
            var continueLoop = true;

            switch (input)
            {
                case "1":
                    ChooseActionWIP();
                    break;
                case "2":
                    CoinFlipSimulator.FlipACoin();
                    break;
                case "3":
                    ChangeReturn.Change();
                    break;
                case "4":
                    CreditCardValidator.ValidateCreditCard();
                    break;
                case "5":
                    FibonacciSequence.Fibonacci();
                    break;
                case "0":
                case "exit":
                case "back":
                    continueLoop = false;
                    break;
                default:
                    Console.WriteLine("Invalid input!");
                    break;
            }
            return continueLoop;
        }

        private static void ChooseActionWIP()
        {
            var continueLoop = true;

            while (continueLoop)
            {
                Console.WriteLine();
                switch (PrintWIPMenu())
                {
                    case "1":
                        var unitConverter = new UnitConverter();
                        unitConverter.Main();
                        break;
                    case "2":
                        Console.WriteLine("Work in progress!");
                        
                        break;
                    case "3":
                        WeatherApplication.Weather();
                        break;
                    case "4":
                        var rngGame = new RNGame();
                        rngGame.Main();
                        break;
                    case "5":
                        var rssReader = new RSSReaderApplication();
                        rssReader.Main();
                        break;
                    case "6":
                        var time = new TimeApplication();
                        time.TellCurrentTime();
                        break;
                    case "7":
                        var leagueApplication = new LeagueManager();
                        leagueApplication.MainProgram();
                        break;
                    case "8":
                        var blackjack = new BlackjackGameApplication();
                        blackjack.Main();
                        break;
                    case "9":
                        var logApp = new LogApplication();
                        logApp.Main();
                        break;
                    case "10":
                        var coffeeApp = new CoffeeShop.CoffeeShopApplication();
                        coffeeApp.Main();
                        break;
                    case "0":
                    case "exit":
                    case "back":
                        continueLoop = false;
                        break;
                    default:
                        Console.WriteLine("Invalid input!");
                        break;
                }
            }
        }

        private static string PrintWIPMenu()
        {
            Console.WriteLine("1) Converter");
            Console.WriteLine("2) Alarm clock (now just a beeb test)");
            Console.WriteLine("3) Get weather");
            Console.WriteLine("4) RngGame");
            Console.WriteLine("5) RSS-reader");
            Console.WriteLine("6) Time application");
            Console.WriteLine("7) League manager");
            Console.WriteLine("8) Blackjack game");
            Console.WriteLine("9) Log application");
            Console.WriteLine("10) Coffee shop");
            Console.WriteLine("0) Back");
            Console.Write("Enter: ");
            return Console.ReadLine().ToLower();
        }

        private static string PrintApplicationsMenu()
        {
            Console.WriteLine();
            Console.WriteLine("1) Work in Progress");
            Console.WriteLine("2) Coin flip simulator");
            Console.WriteLine("3) Change return calculator");
            Console.WriteLine("4) Credit card validator");
            Console.WriteLine("5) Calculate nth number from fibonacci sequence");
            Console.WriteLine("0) Back");
            Console.Write("Enter: ");
            return Console.ReadLine().ToLower();
        }
    }
}
