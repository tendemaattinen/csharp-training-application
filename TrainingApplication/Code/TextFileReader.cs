﻿/*
 * Author: Teemu Tynkkynen
 * Description: Class that prints content of given text file to console. Prints text bewtween given words.
 * 
 */

using System;
using System.IO;

namespace TrainingApplication
{
    class TextFileReader
    {
        public static void ReadTextFile(string startWord, string endWord)
        {
            Console.WriteLine();
            try
            {
                using (StreamReader sR = new StreamReader("Text Files/helpTexts.txt"))
                {
                    bool loop = false;
                    bool loop2 = true;
                    string line;
                    while (((line = sR.ReadLine()) != null) && loop2)
                    {
                        if (loop)
                        {
                            if (line == endWord)
                            {
                                loop = false;
                                loop2 = false;
                            }
                            else
                            {
                                Console.WriteLine(line);
                            }
                        }
                        if (line == startWord)
                        {
                            loop = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
