﻿/*
 * Author: Teemu Tynkkynen
 * Description: Class that handles register process. 
 * 
 */

using System;
using System.Data.SQLite;

namespace TrainingApplication
{
    class Register
    {
        public static void RegisterNewAccount(SQLiteConnection connection)
        {
            SQLiteCommand command;
            string username, passwordOne, passwordTwo;
            Console.WriteLine();

            do
            {
                Console.Write("Enter username:  ");
                username = Console.ReadLine();
            } while (!CheckUsername(username, connection));

            do
            {
                Console.Write("Enter password:  ");
                passwordOne = MaskPassword.Mask();
                Console.Write("\nEnter password again:  ");
                passwordTwo = MaskPassword.Mask();
            } while (!CheckPassWord(passwordOne, passwordTwo));

            var passwordAsHash = Hash.GenerateHash(passwordOne);

            var sql = "INSERT INTO accounts (username, password, isadmin) VALUES (@username, @passwordHash, @admin)";
            command = new SQLiteCommand(sql, connection);
            command.Parameters.AddWithValue("@username", username);
            command.Parameters.AddWithValue("@passwordhash", passwordAsHash);
            command.Parameters.AddWithValue("@admin", 0);
            command.ExecuteNonQuery();

            Console.WriteLine();
            Console.WriteLine("\nSuccessfully registered!");
        }

        private static bool CheckPassWord(string pW1, string pW2)
        {
            var check = false;

            if (pW1 != pW2)
            {
                Console.WriteLine("\nPasswords do not match!");
            }
            else if (pW1.Length < 4)
            {
                Console.WriteLine("\nPasswords is not long enough!");
            }
            else if (pW1.Length >= 50)
            {
                Console.WriteLine("\nPassword too long!");
            }
            else if (string.IsNullOrEmpty(pW1))
            {
                Console.WriteLine("\nPassword can not be empty!");
            }
            else
            {
                check = true;
            }

            return check;
        }

        private static bool CheckUsername(string name, SQLiteConnection conn)
        {
            var check = false;

            if (string.IsNullOrEmpty(name))
            {
                Console.WriteLine("Username can not be empty!");
            }
            else if (name.Length >= 50)
            {
                Console.WriteLine("Username too long!");
            }
            else
            {
                check = true;
            }

            var sql = "SELECT username FROM accounts WHERE username = @un";
            SQLiteCommand command = new SQLiteCommand(sql, conn);
            command.Parameters.AddWithValue("@un", name);

            try
            {
                var column = command.ExecuteScalar();
                if (column != null)
                {
                    check = false;
                    Console.WriteLine("Username is already in use!");
                }
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                check = false;
            }

            return check;
        }
    }
}
