﻿/*
 * Author: Teemu Tynkkynen
 * Description: Log taking application.
 * 
 */

using System;
using System.IO;

namespace TrainingApplication
{
    class LogApplication
    {
        public void Main()
        {
            var continueLoop = true;
            string fileName;

            while (continueLoop)
            {
                var userInput = MainMenu();
                switch (userInput)
                {
                    case "1":
                        fileName = "Text Files/logfile.txt";
                        FileHanding(fileName);
                        break;
                    case "2":
                        Console.WriteLine();
                        Console.WriteLine("Enter file name: ");
                        fileName = Console.ReadLine();
                        FileHanding(fileName);
                        break;
                    case "info":
                    case "help":
                        // Info here.
                        break;
                    case "0":
                    case "back":
                    case "exit":
                        continueLoop = false;
                        break;
                    default:
                        Console.WriteLine("Invalid input");
                        break;
                }
            }
        }

        private string MainMenu()
        {
            Console.WriteLine();
            Console.WriteLine("1) Open default text file");
            Console.WriteLine("2) Open text file");
            Console.WriteLine("0) Back");
            Console.Write("Enter: ");
            var userInput = Console.ReadLine().ToLower();
            return userInput;
        }

        private void ReadTextFile(string name)
        {
            Console.WriteLine();
            using (StreamReader sr = new StreamReader(name))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                { 
                    Console.WriteLine(line);
                }
            }
            Console.WriteLine();
        }

        private void FileHanding(string fileName)
        {
            var loop = true;
            DateTime time;
            try
            {
                ReadTextFile(fileName);
                while (loop)
                {
                    Console.Write("Enter: ");
                    var input = Console.ReadLine();
                    var inputCheck = input.ToLower();

                    time = DateTime.Now;

                    if (inputCheck == "exit" || inputCheck == "back" || inputCheck == "0")
                    {
                        loop = false;
                    }

                    else if (inputCheck == "info" || inputCheck == "help")
                    {
                        // Info here.
                    }

                    else
                    {
                        using (StreamWriter sW = File.AppendText(fileName))
                        {
                            sW.WriteLine($"{time.ToString("yyyy.MM.dd HH:mm:ss")} {input}");
                        }
                    }
                    Console.WriteLine();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}