﻿/*
 * Author: Teemu Tynkkynen
 * Description: Implementation of fibonacci sequence calculation.
 * 
 */

using System;
using System.Collections.Generic;

namespace TrainingApplication
{
    public class FibonacciSequence
    {
        public static void Fibonacci()
        {
            Console.Write("\nEnter a number: ");
            if (long.TryParse(Console.ReadLine(), out long number))
            {
                CalculateNthNumber(number);
            }
            else
            {
                Console.WriteLine("Invalid input!");
            }
        }

        public static ulong CalculateNthNumber(long number)
        {
            IList<ulong> list = new List<ulong>() { 0L, 1L };
            for (int i = 2; i < number + 1; i++)
            {
                list.Add(list[i - 2] + list[i - 1]);
            }

            int counter = 0;
            foreach (var num in list)
            {
                Console.WriteLine("  " + counter + ": " + num);
                counter++;
            }
            return list[list.Count - 1];
        }
    }
}
