﻿/*
 * Author: Teemu Tynkkynen
 * Description: Implemention of coin flip simulator.
 */

using System;

namespace TrainingApplication
{
    class CoinFlipSimulator
    {
        public static void FlipACoin()
        {
            var loop = true;
            Random rand = new Random();
            int numberOfTails, numberOfHeads, counter, coinFlip;

            Console.WriteLine();
            Console.WriteLine("*** Coin flip simulator ***");
            
            while (loop)
            {
                numberOfHeads = 0;
                numberOfTails = 0;
                counter = 0;
                Console.WriteLine();
                Console.Write("Enter amount of flips: ");
                string input = Console.ReadLine().ToLower();

                if (input == "exit" || input == "quit" || input == "0")
                {
                    loop = false;
                }
                else
                {
                    if (int.TryParse(input, out int amountOfFlips))
                    {
                        Console.WriteLine();
                        while (counter < amountOfFlips)
                        {
                            coinFlip = rand.Next(0, 2);
                            // 0 is head.
                            if (coinFlip == 0)
                            {
                                Console.WriteLine("Head");
                                numberOfHeads++;
                            }
                            // 1 is tail.
                            else
                            {
                                Console.WriteLine("Tail");
                                numberOfTails++;
                            }
                            counter++;
                        }

                        double percentageHeads = ((numberOfHeads * 100.00) / amountOfFlips);
                        double percentageTails = (numberOfTails * 100.00) / amountOfFlips;
                        percentageHeads = Math.Round(percentageHeads, 2, MidpointRounding.AwayFromZero);
                        percentageTails = Math.Round(percentageTails, 2, MidpointRounding.AwayFromZero);

                        Console.WriteLine();
                        Console.WriteLine($"Heads: {percentageHeads}% ({numberOfHeads})");
                        Console.WriteLine($"Tails: {percentageTails}% ({numberOfTails})");
                    }
                    else
                    {
                        Console.WriteLine("Invalid input!");
                    }
                }
            }
        }
    }
}
