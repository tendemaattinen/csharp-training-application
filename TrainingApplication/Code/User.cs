﻿
namespace TrainingApplication
{
    class User
    {
        public int UserId { get; }
        public string Username { get; }
        public bool IsAdmin { get; }

        public User(int userId, string username, bool isAdmin)
        {
            UserId = userId;
            Username = username;
            IsAdmin = isAdmin;
        }

        public User()
        {

        }
    }
}
