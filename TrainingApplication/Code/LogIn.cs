﻿/*
 * Author: Teemu Tynkkynen
 * Description: Class that handles log in process.
 * 
 */

using System;
using System.Data.SQLite;

namespace TrainingApplication
{
    class LogIn
    {
        public static User TryToLoginIn(SQLiteConnection connection, Action<bool> isLoginSuccessful)
        {
            string password;
            User user;
            isLoginSuccessful(false);
            Console.WriteLine();
            Console.Write("Enter username: ");
            var userName = Console.ReadLine();
            Console.Write("Enter password: ");

            password = MaskPassword.Mask();
            bool successfulLogIn = false;
            user = CheckMatch(userName, password, connection, (match) => successfulLogIn = match);

            if (successfulLogIn)
            {
                isLoginSuccessful(true);
                Console.WriteLine("\n\nSuccessful login");
            }

            return user;
        }

        private static User CheckMatch(string name, string pW, SQLiteConnection conn, Action<bool> successfulLogin)
        {
            successfulLogin(false);
            var savedPassword = "";
            var userId = 0;
            long admin = 0;
            var isAdmin = false;

            var sql = "SELECT password, isadmin FROM accounts WHERE username = @un";
            SQLiteCommand command = new SQLiteCommand(sql, conn);
            command.Parameters.AddWithValue("@un", name);

            try
            {
                SQLiteDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    savedPassword = (string)reader["password"];
                    admin = (long)reader["isadmin"];
                }
                bool login = Hash.CheckPasswordMatch(savedPassword, pW);
                successfulLogin(login);
                if (!login)
                {
                    Console.WriteLine("\nUsername and password do not match!");
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            if (admin == 1)
            {
                isAdmin = true;
            }

            var user = new User(userId, name, isAdmin);

            return user;
        }
    }
}
