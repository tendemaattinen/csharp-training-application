﻿/*
 * Author: Teemu Tynkkynen
 * Description: Implementation of RSS-Reader application.
 * 
 */

using System;
using System.Xml;
using System.ServiceModel.Syndication;

namespace TrainingApplication
{
    class RSSReaderApplication
    {
        public void Main()
        {
            string url;
            var read = true;

            Console.Write("Enter URL: ");
            url = Console.ReadLine().ToLower();
            // TODO
            // Change to switch statement.
            if (url == "" || url == "exit" || url == "quit" || url == "0")
            {
                Console.WriteLine("Reading terminated.");
                read = false;
            }
            else if (url == "bbc")
            {
                url = "http://feeds.bbci.co.uk/news/rss.xml";
            }
            else if (url == "is")
            {
                url = "https://www.is.fi/rss/tuoreimmat.xml";
            }
            else if (url == "help")
            {
                // TODO Add help text.

                // Disables current method and starts a new Main-method.
                read = false;
                Main();
            }

            // Reads RSS-feed from url, if url is given.
            if (read)
            {
                ReadRSS(url);
            }
        }

        private void ReadRSS(string url)
        {
            try
            {
                int numberOfArticle = 0;
                // Uses Syndication feed to read and parse RSS.
                XmlReader reader = XmlReader.Create(url);
                SyndicationFeed feed = SyndicationFeed.Load(reader);
                reader.Close();
                foreach (SyndicationItem item in feed.Items)
                {
                    String title = item.Title.Text;
                    String summary = item.Summary.Text;
                    String link = item.Links[0].Uri.OriginalString;
                    Console.WriteLine($"\nTitle: {title}");
                    Console.WriteLine($"Summary: {summary}");
                    Console.WriteLine($"Link: {link}");
                    numberOfArticle++;

                    // Pauses printing after five news, user continue by pressing 'Enter'-key.
                    if (numberOfArticle == 5)
                    {
                        Console.WriteLine("\nPress 'Enter' to continue.");
                        Console.ReadKey();
                        numberOfArticle = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
