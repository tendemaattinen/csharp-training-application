﻿/*
 * Author: Teemu Tynkkynen
 * Description: Turn-based rng game.
 * 
 */

using System;
using System.Collections.Generic;
using System.Text;

namespace TrainingApplication
{
    class RNGame
    {
        // Main method.
        public void Main()
        {
            var loop = true;
            Player player;
            Enemy enemy;
            Console.WriteLine();
            Console.WriteLine("*** Game ***");
            Console.WriteLine();
            Console.Write("Enter your heros name: ");

            (player, enemy) = Initialize(Console.ReadLine());

            while (loop)
            {
                (player, loop) = Fight(player, enemy);

                if (loop)
                {
                    player.LevelUp();
                    enemy = CreateNewEnemy(player.Level);
                }
            }
        }

        // Method to initialize game. Creates a new player and first enemy.
        private (Player, Enemy) Initialize(string name)
        {
            var newPlayer = new Player(1000, 1, name);
            var newEnemy = CreateNewEnemy(newPlayer.Level);
            return (newPlayer, newEnemy);
        }

        // Method to create new enemy.
        private Enemy CreateNewEnemy(int level)
        {
            double health = ((1.0 + (level / 10.0)) * 1000.0) - 100.0;
            Console.WriteLine(health);
            int h = Convert.ToInt32(health);
            Console.WriteLine(h);
            var newEnemy = new Enemy(level, h);

            return newEnemy;
        }

        // Method for fighting between player and enemy.
        private (Player, bool) Fight(Player player, Enemy enemy)
        {
            var win = false;
            var loop = true;

            while (loop)
            {
                Console.WriteLine();
                Console.WriteLine($"HP: {player.CurrentHP}      Enemy HP: {enemy.CurrentHP}");
                Console.WriteLine("attack      heal     defence     talk      help    run");
                Console.Write("Enter action: ");
                
               switch (Console.ReadLine().ToLower())
                {
                    case "attack":
                        int dmg = player.Attack();
                        System.Threading.Thread.Sleep(250);
                        enemy.TakeDamage(dmg);
                        break;
                    case "run":
                        Console.WriteLine("Coward player runs away!");
                        loop = false;
                        win = false;
                        break;
                    case "talk":
                        player.Talk();
                        break;
                    case "help":
                        // Print help
                        break;
                    case "defence":
                        player.Defence();
                        break;
                    case "heal":
                        player.Heal();
                        break;
                    default:
                        Console.WriteLine("Invalid action!");
                        break;
                }

                if (loop)
                {
                    Console.WriteLine();
                    int eDmg = enemy.Attack();
                    player.TakeDamage(eDmg);
                }

                // End fight here
                if (player.CurrentHP <= 0)
                {
                    // Lose
                    loop = false;
                    win = false;
                    Console.WriteLine();
                    Console.WriteLine("You lose!");
                }
                else if (enemy.CurrentHP <= 0)
                {
                    // Win
                    loop = false;
                    win = true;
                    Console.WriteLine();
                    Console.WriteLine("You win!");
                }
            }

            return (player, win);
        }
    }

    // Enemy-class.
    class Enemy
    {
        public int CurrentHP { get; set; }
        public int Level { get; set; }
        public double damageMultipier = 1;

        // Constructor.
        public Enemy(int level, int hp)
        {
            Level = level;
            CurrentHP = hp;
        }

        // Method for damage.
        public int Attack()
        {
            var rnd = new Random();
            int damage = rnd.Next(501);
            return damage;
        }

        // Method for taking damage.
        public void TakeDamage(double damage)
        {
            damage = damage * damageMultipier;
            int dmg = Convert.ToInt32(Math.Round(damage));
            CurrentHP = CurrentHP - dmg;
            damageMultipier = 1;
        }

        // Method for defence.
        public void Defence()
        {
            damageMultipier = 0.8;
        }

    }

    // Player-class.
    class Player
    {
        public string Name { get; set; }
        public int CurrentHP { get; set; }
        public int FullHP { get; set; }
        public int Level { get; set; }
        private double damageMultipier = 1;
        private double attackMultipier = 1;
        private int DPG = 1500;
        private int healthPotions;

        // Constructor.
        public Player(int hp, int level, string name)
        {
            FullHP = hp;
            Level = level;
            Name = name;
            CurrentHP = FullHP;
            healthPotions = 3;
        }

        // Method for level up.
        public void LevelUp()
        {
            Level++;
            DPG += 100;
        }

        // Method for healing.
        public void Heal()
        {
            if (healthPotions > 0)
            {
                CurrentHP += 500;
                if (CurrentHP > FullHP)
                {
                    CurrentHP = FullHP;
                }
                healthPotions--;
            }
            else
            {
                Console.WriteLine("You don't have any health potions!");
            }
        }

        // Method for healing.
        public int Attack()
        {
            var rnd = new Random();
            int damage = rnd.Next(DPG);

            damage = Convert.ToInt32(Math.Round(damage * attackMultipier));

            Console.WriteLine($"You deal {damage} to enemy!");
            attackMultipier = 1;

            return damage;
        }

        // Method for taking damage.
        public void TakeDamage(double damage)
        {
            damage = damage * damageMultipier;
            int dmg = Convert.ToInt32(Math.Round(damage));
            CurrentHP = CurrentHP - dmg;
            damageMultipier = 1;
            Console.WriteLine($"Enemy deals {damage} to you!");
        }

        // Method for defence.
        public void Defence()
        {
            damageMultipier = 0.2;
            Console.WriteLine("You increased you defence!");
        }

        // Method for talking.
        public void Talk()
        {
            Console.WriteLine($"{Name}: Hey there!");
            Console.WriteLine("You damaged defence of the enemy!");
            attackMultipier = 1.5;
        }
    }
}
