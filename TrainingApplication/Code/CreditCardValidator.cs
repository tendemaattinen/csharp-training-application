﻿/*
 * Author: Teemu Tynkkynen
 * Description: Implementation of credit card validator application. Uses Luhn algorithm to check
 *              if credit cards number is valid.
 */

using System;

namespace TrainingApplication
{
    public class CreditCardValidator
    {
        public static void ValidateCreditCard()
        {
            bool validityOfCard;
            Console.WriteLine();
            Console.Write("Enter a number of credit card: ");
            var creditCardNumber = Console.ReadLine();
            validityOfCard = LuhnAlgorithm(creditCardNumber);
            if (validityOfCard)
            {
                Console.WriteLine("Card is valid.");
            }
            else
            {
                Console.WriteLine("Card is invalid!");
            }
        }

        // Method to check validity of credit card with Luhn algorithm.
        public static bool LuhnAlgorithm(string number)
        {
            var alternate = false;
            int sum = 0;

            for (int i = (number.Length - 1); i >= 0; i--)
            {
                int n = number[i] - '0';
                if (alternate)
                {
                    n *= 2;
                    if (n > 9)
                    {
                        n = (n % 10) + 1;
                    }
                }
                sum += n;
                alternate = !alternate;
            }

            bool validity = sum % 10 == 0;

            return validity;
        }

    }
}
