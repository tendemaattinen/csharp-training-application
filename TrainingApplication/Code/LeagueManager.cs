﻿/*
 * Author: Teemu Tynkkynen
 * Description: Sport league management application.
 * 
 */

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SQLite;
using System.IO;

namespace TrainingApplication
{
    /// <summary>
    /// Contains basic functions of league manager.
    /// </summary>
    class LeagueManager
    {
        SQLiteConnection conn;

        /// <summary>
        /// Main method, starts program.
        /// </summary>
        public void MainProgram()
        {
            var loop = true;
            League league;

            Console.WriteLine();
            Console.WriteLine("*** League application ***");

            // Initialize application before usage.
            (conn, loop, league) = Initialize();

            while (loop)
            {
                loop = ChooseAction(PrintMenu(), conn, league);
            }

            conn.Close();
        }

        /// <summary>
        /// Prints main menu.
        /// </summary>
        /// <returns>User input as a string.</returns>
        private string PrintMenu()
        {
            Console.WriteLine();
            Console.WriteLine("1) Leaderboard");
            Console.WriteLine("2) New match");
            Console.WriteLine("3) Show");
            Console.WriteLine("4) Edit");
            Console.WriteLine("0) Back");
            Console.Write("Enter: ");
            return Console.ReadLine().ToLower();
        }

        /// <summary>
        /// Initialize application.
        /// </summary>
        /// <returns>Current sqliteconnection as SQLiteConnection, successful connetion as boolean, current league as League object.</returns>
        private (SQLiteConnection, bool, League) Initialize()
        {
            SQLiteConnection connection = null;
            SQLiteCommand command;
            var successfulConnection = true;
            string sql;

            // Create a new league if not exist (new table to save it for later use??)
            League league = new League(3, 1, 0, "Premier League");

            // Check if sqlite database exists, if not, creates a new sqlite database.
            if (!File.Exists("Databases/leagueDb.sqlite"))
            {
                try
                {
                    SQLiteConnection.CreateFile("DataBases/leagueDb.sqlite");

                    connection = new SQLiteConnection("Data Source=Databases/leagueDb.sqlite;Version=3;");

                    connection.Open();

                    sql = "CREATE TABLE teams (id INTEGER PRIMARY KEY, name VARCHAR(50), abbreviation VARCHAR(3), gamesPlayed INTEGER, wins INTEGER, loses INTEGER, draws INTEGER, points INTEGER, goalsFor INTEGER, goalsAgainst INTEGER)";
                    command = new SQLiteCommand(sql, connection);
                    command.ExecuteNonQuery();

                    Console.WriteLine();

                    Console.Write("Create premade league (Premier League 15-16) (y/n): ");
                    var userInput = Console.ReadLine();

                    if (userInput == "y")
                    {
                        CreatePremadeTeams(connection);
                        Console.WriteLine("League created!");
                    }
                    else
                    {
                        // Console.WriteLine("League not made.");
                    }

                    connection.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    successfulConnection = false;
                }
            }

            try
            {
                connection = new SQLiteConnection("Data Source=Databases/leagueDb.sqlite;Version=3;");
                connection.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                successfulConnection = false;
            }

            return (connection, successfulConnection, league);
        }

        /// <summary>
        /// Routes application to right action based on user input.
        /// </summary>
        /// <param name="input">User input as string.</param>
        /// <param name="conn">Current sqlite connection as SQLiteConnection.</param>
        /// <param name="league">Current league as League-object.</param>
        /// <returns>Continue loop as boolean.</returns>
        private bool ChooseAction(string input, SQLiteConnection conn, League league)
        {
            var continueLoop = true;
            switch (input)
            {
                case "1":
                    ShowLeaderboard(conn);
                    break;
                case "2":
                    AddMatch(conn, league);
                    break;
                case "3":
                    ShowPlaceholderName(conn); // Change name!
                    break;
                case "4":
                    EditManager(conn); // Change name!
                    break;
                case "info":
                case "help":
                    // Print help screen!

                    break;
                case "0":
                case "exit":
                case "back":
                    continueLoop = false;
                    break;
                default:
                    Console.WriteLine("Invalid input!");
                    break;
            }
            return continueLoop;
        }

        // Method to show current leaderboard, sorted by points.
        /// <summary>
        /// Shows leaderboard from league in given sqlite connection.
        /// </summary>
        /// <param name="conn">Current sqlite connection as SQLiteConnection.</param>
        private void ShowLeaderboard(SQLiteConnection conn)
        {
            try
            {
                SQLiteCommand command;
                var sql = "SELECT name, abbreviation, points FROM teams ORDER BY points DESC";
                command = new SQLiteCommand(sql, conn);
                SQLiteDataReader reader = command.ExecuteReader();

                Console.WriteLine();

                int i = 1;

                while (reader.Read())
                {
                    string name = (string)reader["name"];
                    string abb = (string)reader["abbreviation"];
                    long points = (long)reader["points"];
                    Console.WriteLine($"{i}) {abb} {name} {points}");
                    i++;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        // Method to add new match to database.
        /// <summary>
        /// Adds new match to given database.
        /// </summary>
        /// <param name="conn">Current sqlite connection as SQLiteConnection.</param>
        /// <param name="league">Current league as League-object.</param>
        private void AddMatch(SQLiteConnection conn, League league)
        {
            SQLiteCommand command;
            int team1Points, team2Points, team1Score = 0, team2Score = 0, points;
            string sql;

            Console.WriteLine();
            Console.Write("Enter: ");
            var userInput = Console.ReadLine();
            var array = userInput.Split(" ");

            // Checks that input is in right format (ABB1 ABB2 0-0 or ABB1 ABB2 random).
            if (array.Length == 3)
            {
                if (array[0].Length == 3 && array[1].Length == 3)
                {
                    if (array[2] == "random" || array[2] == "ran" || array[2] == "r")
                    {
                        Random rand = new Random();

                        team1Score = rand.Next(0, 100);
                        team2Score = rand.Next(0, 100);

                        team1Score = TransferRandomToGoals(team1Score);
                        team2Score = TransferRandomToGoals(team2Score);
                    }
                    else
                    {
                        var array2 = array[2].Split("-");
                        if (array2.Length == 2)
                        {
                            int.TryParse(array2[0], out team1Score);
                            int.TryParse(array2[1], out team2Score);
                        }
                    }

                    var team1 = array[0].ToUpper();
                    var team2 = array[1].ToUpper();

                    // Checks if given teams exists in the database.
                    bool isTeam1found = CheckIfTeamExists(team1, conn);
                    bool isTeam2found = CheckIfTeamExists(team2, conn);

                    // Calculates points if given input is valid and teams are found from database.
                    if (isTeam1found && isTeam2found)
                    {
                        if (team1Score > team2Score)
                        {
                            team1Points = league.GetWinPoints();
                            team2Points = league.GetLosePoints();
                        }
                        else if (team1Score == team2Score)
                        {
                            team1Points = league.GetDrawPoints();
                            team2Points = team1Points;
                        }
                        else
                        {
                            team2Points = league.GetWinPoints();
                            team1Points = league.GetLosePoints();
                        }

                        // Add points to database + add match to database.
                        Console.WriteLine($"{team1}: {team1Points} Goals: {team1Score}");
                        Console.WriteLine($"{team2}: {team2Points} Goals: {team2Score}");

                        // ADD GOALS TO THIS AND OTHER STUFF!

                        sql = "SELECT points FROM teams WHERE abbreviation = @abb";
                        command = new SQLiteCommand(sql, conn);
                        command.Parameters.AddWithValue("@abb", team1);

                        points = Convert.ToInt32(command.ExecuteScalar());

                        points += team1Points;

                        sql = "UPDATE teams SET points = @poi WHERE abbreviation = @abb";
                        command = new SQLiteCommand(sql, conn);
                        command.Parameters.AddWithValue("@abb", team1);
                        command.Parameters.AddWithValue("@poi", points);

                        command.ExecuteNonQuery();

                    }
                    else
                    {
                        Console.WriteLine("Check team abbreviations!");
                    }
                }
                else
                {
                    Console.WriteLine("Invalid input!");
                }
            }
            else
            {
                Console.WriteLine("Invalid input!");
            }
        }

        /// <summary>
        /// Checks if given string exists in the database.
        /// </summary>
        /// <param name="abbreviation">User input as string.</param>
        /// <param name="conn">Current sqlite connection as SQLiteConnection.</param>
        /// <returns></returns>
        private bool CheckIfTeamExists(string abbreviation, SQLiteConnection conn)
        {
            var found = false;
            SQLiteCommand command;

            var sql = "SELECT count(*) FROM teams WHERE abbreviation = @abb";
            command = new SQLiteCommand(sql, conn);
            command.Parameters.AddWithValue("@abb", abbreviation);

            int count = Convert.ToInt32(command.ExecuteScalar());

            if (count == 1)
            {
                found = true;
            }

            return found;
        }

        /// <summary>
        /// Gives number of goals based of given random number.
        /// </summary>
        /// <param name="randomNumber">Random number as integral.</param>
        /// <returns>Number of goals as integer.</returns>
        private int TransferRandomToGoals(int randomNumber)
        {
            int numberOfGoals;

            if (randomNumber >= 0 && randomNumber < 30)
            {
                numberOfGoals = 0;
            }
            else if (randomNumber >= 30 && randomNumber < 60)
            {
                numberOfGoals = 1;
            }
            else if (randomNumber >= 60 && randomNumber < 80)
            {
                numberOfGoals = 2;
            }
            else if (randomNumber >= 80 && randomNumber < 90)
            {
                numberOfGoals = 3;
            }
            else if (randomNumber >= 90 && randomNumber < 95)
            {
                numberOfGoals = 4;
            }
            else if (randomNumber >= 95 && randomNumber < 98)
            {
                numberOfGoals = 5;
            }
            else if (randomNumber >= 98 && randomNumber < 100)
            {
                numberOfGoals = 6;
            }
            else
            {
                numberOfGoals = 7;
            }

            return numberOfGoals;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="conn"></param>
        private void ShowPlaceholderName(SQLiteConnection conn)
        {
            var continueLoop = true;
            while (continueLoop)
            {
                Console.WriteLine();
                Console.WriteLine("1) First");
                Console.WriteLine("2) Second");
                Console.WriteLine("3) Third");
                Console.WriteLine("4) Fourth");
                Console.WriteLine("5) Fifth");
                Console.WriteLine("6) Sixth");
                Console.WriteLine("7) Seventh");
                Console.WriteLine("8) Eight");
                Console.WriteLine("0) Back");
                Console.Write("Enter: ");
                string userInput = Console.ReadLine().ToLower();
                Console.WriteLine();

                switch (userInput)
                {
                    case "1":

                        break;
                    case "2":

                        break;
                    case "3":

                        break;
                    case "4":

                        break;
                    case "5":

                        break;
                    case "6":

                        break;
                    case "0":
                    case "exit":
                    case "quit":
                    case "back":
                        continueLoop = false;
                        break;
                }
            }
        }

        /// <summary>
        /// Settings menu.
        /// </summary>
        /// <param name="conn">Current sqlite connection as SQLiteConnection.</param>
        private void EditManager(SQLiteConnection conn)
        {
            var continueLoop = true;
            while (continueLoop)
            {
                Console.WriteLine();
                Console.WriteLine("1) First");
                Console.WriteLine("2) Second");
                Console.WriteLine("3) Third");
                Console.WriteLine("4) Fourth");
                Console.WriteLine("5) Fifth");
                Console.WriteLine("6) Sixth");
                Console.WriteLine("7) Seventh");
                Console.WriteLine("8) Eight");
                Console.WriteLine("0) Back");
                Console.Write("Enter: ");
                string userInput = Console.ReadLine().ToLower();
                Console.WriteLine();

                switch (userInput)
                {
                    case "1":

                        break;
                    case "2":

                        break;
                    case "3":

                        break;
                    case "4":

                        break;
                    case "5":

                        break;
                    case "6":

                        break;
                    case "0":
                    case "exit":
                    case "quit":
                    case "back":
                        continueLoop = false;
                        break;
                }
            }
        }

        /// <summary>
        /// Creates premade league.
        /// </summary>
        /// <param name="conn">Current sqlite connection as SQLiteConnection.</param>
        private void CreatePremadeTeams(SQLiteConnection conn)
        {
            SQLiteCommand command;
            string sql;

            sql = "INSERT INTO teams (name, abbreviation, abbreviation, wins, loses, draws, points, goalsFor, goalsAgainst) VALUES ('Arsenal', 'ARS', 0, 0, 0, 0, 22, 0, 0)";
            command = new SQLiteCommand(sql, conn);
            command.ExecuteNonQuery();

            sql = "INSERT INTO teams (name, abbreviation, gamesPlayed, wins, loses, draws, points, goalsFor, goalsAgainst) VALUES ('Aston Villa', 'AST', 0, 0, 0, 0, 3, 0, 0)";
            command = new SQLiteCommand(sql, conn);
            command.ExecuteNonQuery();

            sql = "INSERT INTO teams (name, abbreviation, abbreviation, wins, loses, draws, points, goalsFor, goalsAgainst) VALUES ('Bournemouth', 'BOU', 0, 0, 0, 0, 25, 0, 0)";
            command = new SQLiteCommand(sql, conn);
            command.ExecuteNonQuery();

            sql = "INSERT INTO teams (name, abbreviation, abbreviation, wins, loses, draws, points, goalsFor, goalsAgainst) VALUES ('Chelsea', 'CHE', 0, 0, 0, 0, 1, 0, 0)";
            command = new SQLiteCommand(sql, conn);
            command.ExecuteNonQuery();

            sql = "INSERT INTO teams (name, abbreviation, abbreviation, wins, loses, draws, points, goalsFor, goalsAgainst) VALUES ('Crystal Palace', 'CRY', 0, 0, 0, 0, 1, 0, 0)";
            command = new SQLiteCommand(sql, conn);
            command.ExecuteNonQuery();

            sql = "INSERT INTO teams (name, abbreviation, abbreviation, wins, loses, draws, points, goalsFor, goalsAgainst) VALUES ('Everton', 'EVE', 0, 0, 0, 0, 1, 0, 0)";
            command = new SQLiteCommand(sql, conn);
            command.ExecuteNonQuery();

            sql = "INSERT INTO teams (name, abbreviation, abbreviation, wins, loses, draws, points, goalsFor, goalsAgainst) VALUES ('Leicester City', 'LEI', 0, 0, 0, 0, 1, 0, 0)";
            command = new SQLiteCommand(sql, conn);
            command.ExecuteNonQuery();

            sql = "INSERT INTO teams (name, abbreviation, abbreviation, wins, loses, draws, points, goalsFor, goalsAgainst) VALUES ('Liverpool', 'LIV', 0, 0, 0, 0, 16, 0, 0)";
            command = new SQLiteCommand(sql, conn);
            command.ExecuteNonQuery();

            sql = "INSERT INTO teams (name, abbreviation, abbreviation, wins, loses, draws, points, goalsFor, goalsAgainst) VALUES ('Manchester City', 'MCI', 0, 0, 0, 0, 0, 0, 0)";
            command = new SQLiteCommand(sql, conn);
            command.ExecuteNonQuery();

            sql = "INSERT INTO teams (name, abbreviation, abbreviation, wins, loses, draws, points, goalsFor, goalsAgainst) VALUES ('Manchester United', 'MAN', 0, 0, 0, 0, 50, 0, 0)";
            command = new SQLiteCommand(sql, conn);
            command.ExecuteNonQuery();

            sql = "INSERT INTO teams (name, abbreviation, abbreviation, wins, loses, draws, points, goalsFor, goalsAgainst) VALUES ('Newcastle United', 'NEW', 0, 0, 0, 0, 23, 0, 0)";
            command = new SQLiteCommand(sql, conn);
            command.ExecuteNonQuery();

            sql = "INSERT INTO teams (name, abbreviation, abbreviation, wins, loses, draws, points, goalsFor, goalsAgainst) VALUES ('Norwich City', 'NOR', 0, 0, 0, 0, 45, 0, 0)";
            command = new SQLiteCommand(sql, conn);
            command.ExecuteNonQuery();

            sql = "INSERT INTO teams (name, abbreviation, abbreviation, wins, loses, draws, points, goalsFor, goalsAgainst) VALUES ('Southampton', 'SOU', 0, 0, 0, 0, 12, 0, 0)";
            command = new SQLiteCommand(sql, conn);
            command.ExecuteNonQuery();

            sql = "INSERT INTO teams (name, abbreviation, abbreviation, wins, loses, draws, points, goalsFor, goalsAgainst) VALUES ('Stoke City', 'STK', 0, 0, 0, 0, 33, 0, 0)";
            command = new SQLiteCommand(sql, conn);
            command.ExecuteNonQuery();

            sql = "INSERT INTO teams (name, abbreviation, abbreviation, wins, loses, draws, points, goalsFor, goalsAgainst) VALUES ('Sunderland', 'SUN', 0, 0, 0, 0, 12, 0, 0)";
            command = new SQLiteCommand(sql, conn);
            command.ExecuteNonQuery();

            sql = "INSERT INTO teams (name, abbreviation, abbreviation, wins, loses, draws, points, goalsFor, goalsAgainst) VALUES ('Swansea City', 'SWA', 0, 0, 0, 0, 9, 0, 0)";
            command = new SQLiteCommand(sql, conn);
            command.ExecuteNonQuery();

            sql = "INSERT INTO teams (name, abbreviation, abbreviation, wins, loses, draws, points, goalsFor, goalsAgainst) VALUES ('Tottenham Hotspur', 'TOT', 0, 0, 0, 0, 3, 0, 0)";
            command = new SQLiteCommand(sql, conn);
            command.ExecuteNonQuery();

            sql = "INSERT INTO teams (name, abbreviation, abbreviation, wins, loses, draws, points, goalsFor, goalsAgainst) VALUES ('Watford', 'WAT', 0, 0, 0, 0, 25, 0, 0)";
            command = new SQLiteCommand(sql, conn);
            command.ExecuteNonQuery();

            sql = "INSERT INTO teams (name, abbreviation, abbreviation, wins, loses, draws, points, goalsFor, goalsAgainst) VALUES ('West Bromwich Albion', 'WBA', 0, 0, 0, 0, 43, 0, 0)";
            command = new SQLiteCommand(sql, conn);
            command.ExecuteNonQuery();

            sql = "INSERT INTO teams (name, abbreviation, abbreviation, wins, loses, draws, points, goalsFor, goalsAgainst) VALUES ('West Ham United', 'WHU', 0, 0, 0, 0, 49, 0, 0)";
            command = new SQLiteCommand(sql, conn);
            command.ExecuteNonQuery();

        }
    }

    /// <summary>
    /// League class, contains league specific information.
    /// </summary>
    class League
    {
        private int pointsWin;
        private int pointsDraw;
        private int pointsLose;
        private string leagueName;

        /// <summary>
        /// Constructor for league.
        /// </summary>
        /// <param name="pW">How many points for a win as integer.</param>
        /// <param name="pD">How many points for a draw as intege.r</param>
        /// <param name="pL">How many points for a lose as integer.</param>
        /// <param name="name">Leagues name as string.</param>
        public League(int pW, int pD, int pL, string name)
        {
            pointsWin = pW;
            pointsDraw = pD;
            pointsLose = pL;
            leagueName = name;
        }

        /// <summary>
        /// Gets number of points from win.
        /// </summary>
        /// <returns>points for win as integer.</returns>
        public int GetWinPoints()
        {
            return pointsWin;
        }

        /// <summary>
        /// Gets number of points from draw.
        /// </summary>
        /// <returns>Points for draw as integer.</returns>
        public int GetDrawPoints()
        {
            return pointsDraw;
        }

        /// <summary>
        /// Gets number of points from lose.
        /// </summary>
        /// <returns>Points for lose as integer.</returns>
        public int GetLosePoints()
        {
            return pointsLose;
        }

        /// <summary>
        /// Gets leagues name.
        /// </summary>
        /// <returns>Leagues name as string.</returns>
        public string GetLeagueName()
        {
            return leagueName;
        }
    }
}
