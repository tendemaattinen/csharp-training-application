﻿/*
 * Author: Teemu Tynkkynen
 * Description: Implementation of weather application.
 */

using System;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Net;
using System.Text;

namespace TrainingApplication
{
    class WeatherApplication
    {
        public static void Weather()
        {
            var loop = true;

            Console.WriteLine("\n*** Weather ***");

            while (loop)
            {
                loop = ChooseAction(PrintMenu());
            }
        }

        private static string PrintMenu()
        {
            Console.WriteLine("\n1) Current weather");
            Console.WriteLine("2) Future weather (WIP)");
            Console.WriteLine("0) Back");
            Console.Write("Enter: ");
            return Console.ReadLine().ToLower();
        }

        private static bool ChooseAction(string input)
        {
            var continueLoop = true;

            switch (input)
            {
                case "1":
                    ShowCurrentWeather();
                    break;
                case "2":
                    ShowFutureWeather();
                    break;
                case "help":
                case "info":
                    // TODO Add help file.
                    Console.WriteLine("Here should be help.");
                    break;
                case "0":
                case "exit":
                case "back":
                    continueLoop = false;
                    break;
                default:
                    Console.WriteLine("Invalid input!");
                    break;
            }
            return continueLoop;
        }

        private static void ShowCurrentWeather()
        {
            var cityName = GetCityName();
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("https://api.openweathermap.org/data/2.5/weather?q=");
                sb.Append(cityName);
                sb.Append("&APPID=8c2f5151d074fe5f55397d8a526d4fe4");
                var url = sb.ToString();

                using (WebClient wc = new WebClient())
                {
                    var json = wc.DownloadString(url);

                    JObject obj = JObject.Parse(json);

                    var weatherMain = obj.Descendants().OfType<JProperty>().First(p => p.Name == "main").Value.ToString();
                    var weatherMainDescription = obj.Descendants().OfType<JProperty>().First(p => p.Name == "description").Value.ToString();
                    var temperature = obj.Descendants().OfType<JProperty>().First(p => p.Name == "temp").Value.ToString();
                    double.TryParse(temperature, out double tempAsDouble);
                    var tempAsCelcius = Math.Round((tempAsDouble - 273.15),1);

                    Console.WriteLine($"\n{weatherMainDescription}, temp: {tempAsCelcius} C");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        // TODO Add future weather.
        private static void ShowFutureWeather()
        {
            var cityName = GetCityName();

            /*try
            {
                //var url = "https://api.openweathermap.org/data/2.5/forecast?q=" + cityName + "&APPID=8c2f5151d074fe5f55397d8a526d4fe4";

                // Opens new webclient to Open Weathers json.
                using (WebClient wc = new WebClient())
                {
                    var json = wc.DownloadString(url);

                    JObject obj = JObject.Parse(json);

                    // Change these to match json
                    var weatherMain = obj.Descendants().OfType<JProperty>().First(p => p.Name == "main").Value.ToString();
                    var weatherMainDescription = obj.Descendants().OfType<JProperty>().First(p => p.Name == "description").Value.ToString();
                    var temperature = obj.Descendants().OfType<JProperty>().First(p => p.Name == "temp").Value.ToString();
                    double.TryParse(temperature, out double tempAsDouble);
                    var tempAsCelcius = Math.Round((tempAsDouble - 273.15), 1);


                    Console.WriteLine($"{weatherMainDescription}, temp: {tempAsCelcius} C");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }*/
        }

        private static string GetCityName()
        {
            Console.Write("Enter city: ");
            return Console.ReadLine().ToLower();
        }
    }
}
