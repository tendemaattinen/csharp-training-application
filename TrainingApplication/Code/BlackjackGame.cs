﻿/*
 * Author: Teemu Tynkkynen
 * Description: Implementation of blackjack game.
 * 
 */

using System;
using System.Collections.Generic;

namespace TrainingApplication
{
    class BlackjackGameApplication
    {
        public void Main()
        {
            var continueLoop = true;
            var game = new BlackjackGame();
            game.InitializeGame();

            while (continueLoop)
            {
                switch (PrintMenu())
                {
                    case "1":
                        NewGame(game);
                        break;
                    case "2":
                        PrintRulesAndCommands();
                        break;
                    case "3":
                        EditMenu();
                        break;
                    case "help":
                    case "info":
                        
                        break;
                    case "0":
                    case "exit":
                    case "back":
                        continueLoop = false;
                        break;
                    default:
                        Console.WriteLine("Invalid input!");
                        break;
                }
            }
        }

        private string PrintMenu()
        {
            Console.WriteLine();
            Console.WriteLine("1) New game");
            Console.WriteLine("2) Rules & Commands");
            Console.WriteLine("3) Settings");
            Console.WriteLine("0) Exit");
            Console.Write("Enter: ");
            var input = Console.ReadLine();
            Console.WriteLine();
            return input;
        }

        private void PrintRulesAndCommands()
        {
            Console.WriteLine();
            Console.WriteLine("Rules:");
            Console.WriteLine("   General blackjack rules.");
            Console.WriteLine("   Default is two decks.");
            Console.WriteLine("   Suffling happens when half of the cards in the deck are played.");
            Console.WriteLine("   Distance 3 spaces!");
            Console.WriteLine();
            Console.WriteLine("Commands:");
            Console.WriteLine("   Set a bet -screen:");
            Console.WriteLine("      exit - Exit game.");
            Console.WriteLine("      balance - Show account balance.");
            Console.WriteLine("      add xxx - Add xxx amount to account.");
            Console.WriteLine("   Game -screen:");
            Console.WriteLine("      hit - Hit.");
            Console.WriteLine("      stay - Stay.");
            Console.WriteLine("      double - Double.");
            Console.WriteLine("      back - Go back.");
        }

        private void EditMenu()
        {
            var loop = true;

            while (loop)
            {
                Console.WriteLine("This is edit menu");
                Console.WriteLine("Enter your invalid input: ");
                var userInput = Console.ReadLine().ToLower();

                switch (userInput)
                {
                    case "0":

                        break;
                    default:
                        loop = false;
                        break;
                }
            }
        }

        private void NewGame(BlackjackGame game)
        {
            var loop = true;
            var cardsPlayed = 0;
            var cardsInTheDeck = game.GetLenghtOfTheDeck();
            var accountBalance = 1000.00;
            bool isWin, isDouble;

            Console.WriteLine("Welcome!");

            while (loop)
            {
                Console.WriteLine();
                Console.Write("Set a bet: ");
                string betInput = Console.ReadLine().ToLower();
                Console.WriteLine();
                if (betInput == "exit" || betInput == "back" || betInput == "0")
                {
                    loop = false;
                }
                else if (betInput == "balance")
                {
                    Console.WriteLine($"Balance: {accountBalance}");
                }
                else if (betInput.Contains("add"))
                {
                    var betArray = betInput.Split(" ");
                    try
                    {
                        if (betArray[1] != null)
                        {
                            var isNumber = double.TryParse(betArray[1], out double betIncrease);
                            if (isNumber)
                            {
                                accountBalance += betIncrease;
                                Console.WriteLine("Account balance increased!");
                            }
                            else
                            {
                                Console.WriteLine("Input is not number!");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                }
                else
                {
                    var isNumber = double.TryParse(betInput, out double bet);

                    if (isNumber)
                    {
                        if (accountBalance - bet < 0)
                        {
                            Console.WriteLine("Not enough money!");
                        }
                        else
                        {
                            accountBalance -= bet;

                            (isWin, cardsPlayed, isDouble) = PlayRound(cardsPlayed, game);
                            Console.WriteLine();

                            // User wins a round.
                            if (isWin)
                            {
                                if (isDouble)
                                {
                                    accountBalance -= bet;
                                    accountBalance += (2 * bet)  * 2;
                                    bet = bet * 2;
                                }
                                else
                                {
                                    accountBalance += (2 * bet);
                                }
                                Console.WriteLine($"You won {bet}$!");
                                Console.WriteLine($"Account balance: {accountBalance}$");
                            }
                            // User loses a round.
                            else
                            {
                                if (isDouble)
                                {
                                    accountBalance -= bet;
                                    bet = bet * 2;
                                }
                                Console.WriteLine($"You lost {bet}$!");
                                Console.WriteLine($"Account balance: {accountBalance}$");
                            }

                            // Suffles deck if half of the cards are used.
                            if (cardsPlayed > (cardsInTheDeck / 2))
                            {
                                Console.WriteLine();
                                game.Shuffle();
                                cardsPlayed = 0;
                                Console.WriteLine("Cards suffled!");
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("Invalid input!");
                    }
                }
            }
        }

        private (bool, int, bool) PlayRound(int cardNumber, BlackjackGame game)
        {
            // List for houses cards.
            List<int> houseCards = new List<int>();
            // List for players cards.
            List<int> playerCards = new List<int>();

            var win = false;
            var isDouble = false;

            playerCards.Add(game.DrawACard(cardNumber));
            cardNumber++;
            houseCards.Add(game.DrawACard(cardNumber));
            cardNumber++;
            playerCards.Add(game.DrawACard(cardNumber));
            cardNumber++;
            houseCards.Add(game.DrawACard(cardNumber));
            cardNumber++;

            Console.WriteLine($"Dealer: X {houseCards[1]}");
            Console.WriteLine($"Player: {playerCards[0]} {playerCards[1]}");

            var sumOfPlayersCards = SumOfCards(playerCards);

            var continueLoop = false;

            (continueLoop, win) = CheckPlayerWin(sumOfPlayersCards);

            // Game hasn't ended yet.
            if (continueLoop)
            {
                var loop = true;
                while (loop)
                {
                    Console.Write("Enter: ");
                    string userInput = Console.ReadLine().ToLower();
                    Console.WriteLine();
                    switch (userInput)
                    {
                        case "hit":
                            playerCards.Add(game.DrawACard(cardNumber));
                            cardNumber++;
                            sumOfPlayersCards = SumOfCards(playerCards);

                            Console.WriteLine($"Dealer: X {houseCards[1]}");
                            Console.Write($"Player:");

                            foreach (var card in playerCards)
                            {
                                Console.Write($" {card}");
                            }

                            Console.WriteLine();

                            (loop, win) = CheckPlayerWin(sumOfPlayersCards);
                            break;
                        case "stay":
                            var sumOfHousesCards = SumOfCards(houseCards);

                            Console.WriteLine($"Dealer: {houseCards[0]} {houseCards[1]}");
                            Console.Write($"Player:");

                            foreach (var card in playerCards)
                            {
                                Console.Write($" {card}");
                            }

                            Console.WriteLine();

                            (loop, win) = CheckHouseWin(sumOfHousesCards, sumOfPlayersCards);

                            // Loop for dealer to draw cards until he has higher count than user or busts.
                            while (loop)
                            {
                                if (sumOfHousesCards <= sumOfPlayersCards)
                                {
                                    houseCards.Add(game.DrawACard(cardNumber));
                                    cardNumber++;

                                    Console.WriteLine();

                                    Console.Write($"Dealer:");
                                    foreach (var card in houseCards)
                                    {
                                        Console.Write($" {card}");
                                    }
                                    Console.WriteLine();

                                    Console.Write($"Player:");
                                    foreach (var card in playerCards)
                                    {
                                        Console.Write($" {card}");
                                    }
                                    Console.WriteLine();

                                    (loop, win) = CheckHouseWin(SumOfCards(houseCards), sumOfPlayersCards);

                                }
                            }
                            loop = false;
                            break;
                        case "double":
                            isDouble = true;
                            playerCards.Add(game.DrawACard(cardNumber));
                            cardNumber++;
                            sumOfPlayersCards = SumOfCards(playerCards);

                            Console.WriteLine($"Dealer: X {houseCards[1]}");
                            Console.Write($"Player:");

                            foreach (var card in playerCards)
                            {
                                Console.Write($" {card}");
                            }

                            Console.WriteLine();

                            (loop, win) = CheckPlayerWin(sumOfPlayersCards);

                            sumOfHousesCards = SumOfCards(houseCards);

                            Console.WriteLine();

                            Console.WriteLine($"Dealer: {houseCards[0]} {houseCards[1]}");
                            Console.Write($"Player:");

                            foreach (var card in playerCards)
                            {
                                Console.Write($" {card}");
                            }
                            Console.WriteLine();

                            (loop, win) = CheckHouseWin(sumOfHousesCards, sumOfPlayersCards);

                            // Loop for dealer to draw cards until he has higher count than user or busts.
                            while (loop)
                            {
                                if (sumOfHousesCards <= sumOfPlayersCards)
                                {
                                    houseCards.Add(game.DrawACard(cardNumber));
                                    cardNumber++;

                                    Console.Write($"Dealer:");
                                    foreach (var card in houseCards)
                                    {
                                        Console.Write($" {card}");
                                    }
                                    Console.WriteLine();

                                    Console.Write($"Player:");
                                    foreach (var card in playerCards)
                                    {
                                        Console.Write($" {card}");
                                    }
                                    Console.WriteLine();

                                    (loop, win) = CheckHouseWin(SumOfCards(houseCards), sumOfPlayersCards);
                                }
                            }

                            loop = false;
                            break;
                        case "exit":
                        case "back":
                            win = false;
                            loop = false;
                            break;
                        default:
                            Console.WriteLine("Invalid input!");
                            break;
                    }
                }
            }
            return (win, cardNumber, isDouble);
        }

        private (bool, bool) CheckPlayerWin(int sum)
        {
            var loop = true;
            var win = false;
            if (sum == 21)
            {
                Console.WriteLine();
                Console.WriteLine("Blackjack!");
                Console.WriteLine("Player wins!");
                win = true;
                loop = false;
            }
            else if (sum > 21)
            {
                Console.WriteLine();
                Console.WriteLine("Busted!");
                Console.WriteLine("House wins!");
                loop = false;
            }
            return (loop, win);
        }

        private (bool, bool) CheckHouseWin(int sum, int sumOfPlayersCards)
        {
            var loop = true;
            var win = false;
            if (sum == 21)
            {
                Console.WriteLine();
                Console.WriteLine("Blackjack!");
                Console.WriteLine("House wins!");
                loop = false;
            }
            else if (sum > 21)
            {
                Console.WriteLine();
                Console.WriteLine("Busted!");
                Console.WriteLine("Player wins!");
                win = true;
                loop = false;
            }
            else if (sum > sumOfPlayersCards)
            {
                Console.WriteLine();
                Console.WriteLine("House wins!");
                loop = false;
            }
            return (loop, win);
        }

        private int SumOfCards(List<int> list)
        {
            int sum = 0;

            foreach (var card in list)
            {
                sum += card;
            }

            return sum;
        }
    }

    class BlackjackGame
    {
        
        // Ace = 1, 2 = 2, 3 = 3, 4 = 4, 5 = 5, 6 = 6, 7 = 7, 8 = 8, 9 = 9, 10 = 10, Jack = 11, Queen = 12, King = 13;

        private IList<int> BaseDeck = new List<int>() { 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 6, 6, 6, 6,
        7, 7, 7, 7, 8, 8, 8, 8, 9, 9, 9, 9, 10, 10, 10, 10, 11, 11, 11, 11, 12, 12, 12, 12, 13, 13, 13, 13};
        

        private IList<int> TwoDeck = new List<int>() { 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 6, 6, 6, 6,
        7, 7, 7, 7, 8, 8, 8, 8, 9, 9, 9, 9, 10, 10, 10, 10, 11, 11, 11, 11, 12, 12, 12, 12, 13, 13, 13, 13, 1, 1, 1, 1, 2, 2,
        2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 6, 6, 6, 6, 7, 7, 7, 7, 8, 8, 8, 8, 9, 9, 9, 9, 10, 10, 10, 10,
        11, 11, 11, 11, 12, 12, 12, 12, 13, 13, 13, 13};

        private Random rnd;

        public void InitializeGame()
        {
            Random random = new Random();
            rnd = random;
            Shuffle();
        }

        public void Shuffle()
        {
            for (var i = 0; i < TwoDeck.Count; i++)
            {
                Swap(TwoDeck, i, rnd.Next(i, TwoDeck.Count));
            }
        }

        private void Swap(IList<int> list, int i, int j)
        {
            var temp = list[i];
            list[i] = list[j];
            list[j] = temp;
        }

        public int DrawACard(int number)
        {
            return TwoDeck[number];
        }

        public void SetDeck()
        {

        }

        public int GetLenghtOfTheDeck()
        {
            return TwoDeck.Count;
        }
    }
}