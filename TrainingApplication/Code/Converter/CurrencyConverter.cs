﻿/*
 * Author: Teemu Tynkkynen
 * Description: Implementation of currency converter application.
 * Sources: Currency rates are from https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml
 */

using System;
using System.Xml.Linq;
using System.Linq;

namespace TrainingApplication
{
    public class CurrencyConverter
    {
        public static void Convert()
        {
            var loop = true;
            var url = "https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml";

            Console.WriteLine("\n*** Currency converter ***");
            while (loop)
            {
                Console.Write("\nEnter: ");
                loop = ChooseAction(Console.ReadLine().ToLower(), url);
            }
        }

        private static bool ChooseAction(string input, string xml)
        {
            var continueLoop = true;
            bool check, checkRates;
            string from, to;
            double amount, fromRate, toRate;

            if (input == "0" || input == "exit" || input == "quit")
            {
                continueLoop = false;
            }
            else if (input == "help" || input == "info")
            {
                TextFileReader.ReadTextFile("currencyConverterHelpStart", "currencyConverterHelpEnd");
            }
            else if (input == "all")
            {
                Console.WriteLine();
                ReadAllValues(xml);
            }
            else if (input == "save")
            {
                // TODO Save rates to file.
                Console.WriteLine("Future feature!");
            }
            else
            {
                (amount, from, to, check) = ConvertStringToUnits.Convert(input);
                if (check)
                {
                    (checkRates, fromRate, toRate) = GetCurrencyValues(xml, from, to);
                    if (checkRates)
                    {
                        Console.WriteLine($"Answer: {Math.Round(CalculateValue(amount, fromRate, toRate), 2)} {to}");
                    }
                }
                else
                {
                    Console.WriteLine("Invalid input!");
                }
            }

            return continueLoop;
        }

        private static (bool, double, double) GetCurrencyValues(string xml, string from, string to)
        {
            var toRate = 0.0;
            var fromRate = 0.0;

            var check = false;
            var counterForCheck = 0;

            from = from.ToUpper();
            to = to.ToUpper();

            try
            {
                XDocument doc = XDocument.Load(xml);
                XNamespace gesmesNS = "http://www.gesmes.org/xml/2002-08-01";
                XNamespace defaultNS = "http://www.ecb.int/vocabulary/2002-08-01/eurofxref";

                var cubes = doc.Descendants(defaultNS + "Cube")
                            .Where(x => x.Attribute("currency") != null)
                            .Select(x => new {
                                Currency = (string)x.Attribute("currency"),
                                Rate = (decimal)x.Attribute("rate")
                            }
                );

                // Euro is base value in rates, so it ain't in xml-file.
                if (from == "EUR")
                {
                    fromRate = 1;
                    counterForCheck++;
                }
                if (to == "EUR")
                {
                    toRate = 1;
                    counterForCheck++;
                }

                foreach (var result in cubes)
                {
                    if (result.Currency == from)
                    {
                        double.TryParse(result.Rate.ToString(), out fromRate);
                        counterForCheck++;
                    } 
                    else if (result.Currency == to)
                    {
                        double.TryParse(result.Rate.ToString(), out toRate);
                        counterForCheck++;
                    }
                }

                if (counterForCheck == 2)
                {
                    check = true;
                }
                else
                {
                    Console.WriteLine("Invalid input!");
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return (check, fromRate, toRate);
        }

        private static void ReadAllValues(string xml)
        {
            try
            {
                XDocument doc = XDocument.Load(xml);
                XNamespace gesmesNS = "http://www.gesmes.org/xml/2002-08-01";
                XNamespace defaultNS = "http://www.ecb.int/vocabulary/2002-08-01/eurofxref";

                var cubes = doc.Descendants(defaultNS + "Cube")
                            .Where(x => x.Attribute("currency") != null)
                            .Select(x => new {
                                Currency = (string)x.Attribute("currency"),
                                Rate = (decimal)x.Attribute("rate")
                            }
                );

                foreach (var result in cubes)
                {
                    Console.WriteLine("{0}: {1}", result.Currency, result.Rate);
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static double CalculateValue(double amount, double fromRate, double toRate)
        {
            double answer = amount / fromRate;
            answer *= toRate;
            return answer;
        }
    }
}
