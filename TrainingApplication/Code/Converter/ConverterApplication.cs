﻿/*
 * Author: Teemu Tynkkynen
 * Description: Class that handles unit converting.
 * 
 */

using System;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Linq;

namespace TrainingApplication
{
    class ConverterApplication
    {
        public void Main(string name, string startWord, string endWord, string category)
        {
            var swi = true;
            bool checkArray, checkUnits;
            double number, factor1, factor2;
            string fromUnit, toUnit;

            Console.WriteLine();
            Console.WriteLine(name);

            while (swi)
            {
                Console.WriteLine();
                Console.Write("Enter: ");
                var userInput = Console.ReadLine().ToLower();
                if (userInput == "0" || userInput == "exit" || userInput == "quit")
                {
                    swi = false;
                }
                else if (userInput == "help" || userInput == "info")
                {
                    TextFileReader.ReadTextFile(startWord, endWord);
                }
                else
                {
                    (number, fromUnit, toUnit, checkArray) = ConvertStringToUnits.Convert(userInput);
                    if (checkArray)
                    {
                        (checkUnits, factor1, factor2) = GetFactors(fromUnit, toUnit);
                        if (checkUnits)
                        {
                            ChooseCategory(category, number, factor1, factor2, toUnit);
                        }
                        else
                        {
                            Console.WriteLine("Wrong values!");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Invalid input!");
                    }
                }
            }
        }

        private (bool, double, double) GetFactors(string unit1, string unit2)
        {
            var check = true;

            var factor1 = 0.0;
            var factor2 = 0.0;

            try
            {
                JObject obj = JObject.Parse(File.ReadAllText(@"Code/Converter/ConvertFactors.json"));

                var factor1AsString = obj.Descendants().OfType<JProperty>().First(p => p.Name == unit1).Value.ToString();
                var factor2AsString = obj.Descendants().OfType<JProperty>().First(p => p.Name == unit2).Value.ToString();

                double.TryParse(factor1AsString, out factor1);
                double.TryParse(factor2AsString, out factor2);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                check = false;
            }
            return (check, factor1, factor2);
        }

        private void ChooseCategory(string category, double number, double factor1, double factor2, string toUnit)
        {
            switch (category)
            {
                case "length":
                    Console.WriteLine($"Answer: {CalculateLenght(number, factor1, factor2)} {toUnit}");
                    break;
                case "speed":
                    Console.WriteLine($"Answer: {CalculateSpeed(number, factor1, factor2)} {toUnit}");
                    break;
                default:
                    Console.WriteLine("This should not happen!");
                    break;
            }
        }

        // Method to calculate right length.
        private double CalculateLenght(double baseNumber, double factor1, double factor2)
        {
            var number = baseNumber / factor1;
            number = number * factor2;
            return number;
        }

        // Method to calculate right speed.
        private double CalculateSpeed(double baseNumber, double factor1, double factor2)
        {
            var number = baseNumber / factor1;
            number = number * factor2;
            return number;
        }
    }
}
