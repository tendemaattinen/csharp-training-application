﻿/*
 * Author: Teemu Tynkkynen
 * Description: Class that implements unit converter application.
 * 
 */

using System;

namespace TrainingApplication
{
    class UnitConverter
    {
        // Main method.
        public void Main()
        {
            var loop = true;
            Console.WriteLine();
            Console.WriteLine("*** Unit converter ***");
            while (loop)
            {
                PrintMenu();
                Console.Write("Enter: ");
                var userInput = Console.ReadLine();
                loop = ChooseAction(userInput);
            }
        }

        // Method to print menu.
        private void PrintMenu()
        {
            Console.WriteLine();
            Console.WriteLine("1) Currency");
            Console.WriteLine("2) Length");
            Console.WriteLine("3) Speed (WIP)");
            Console.WriteLine("0) Back");
        }

        // Method to choose right action.
        private bool ChooseAction(string input)
        {
            var continueLoop = true;

            switch (input)
            {
                case "1":
                    CurrencyConverter.Convert();
                    break;
                case "2":
                    var length = new ConverterApplication();
                    length.Main("*** Length ***", "converterLengthHelpStart", "converterLengthHelpEnd", "length");
                    break;
                case "3":
                    var speed = new ConverterApplication();
                    speed.Main("*** Speed ***", "converterSpeedHelpStart", "converterSpeedHelpEnd", "speed");
                    break;
                case "info":
                case "help":
                    // Info here.
                    break;
                case "0":
                    continueLoop = false;
                    break;
                default:
                    Console.WriteLine("Invalid input!");
                    break;
            }

            return continueLoop;
        }
    }
}
