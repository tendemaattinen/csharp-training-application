﻿/*
 * Author: Teemu Tynkkynen
 * Description: Class that converts given string to right units.
 * 
 */

using System;

namespace TrainingApplication
{
    class ConvertStringToUnits
    {
        public static (double, string, string, bool) Convert(string input)
        {
            double number = 0;
            var fromUnit = "";
            var toUnit = "";
            var check = true;

            var array = input.Split(" ");
            if (array.Length < 3 || array.Length > 3)
            {
                if (array.Length == 4 && (array[2].ToLower() == "to"))
                {
                    array[2] = array[3];
                }
                else
                {
                    check = false;
                }
            }
            
            if (check)
            {
                var numberAsString = array[0].ToString().ToLower();
                try
                {
                    double.TryParse(numberAsString, out number);
                    fromUnit = array[1].ToString().ToLower();
                    toUnit = array[2].ToString().ToLower();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    check = false;
                }  
            }
            return (number, fromUnit, toUnit, check);
        }
    }
}
