﻿/*
 * Author: Teemu Tynkkynen
 * Description: Handles time based methods.
 * 
 */

using System;
using System.Collections.Generic;
using System.Text;

namespace TrainingApplication
{
    class TimeApplication
    {
        public void TellCurrentTime()
        {
            string date = DateTime.Now.ToString();

            Console.WriteLine(date);
        }
    }
}
