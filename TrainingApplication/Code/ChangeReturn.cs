﻿/*
 * Author: Teemu Tynkkynen
 * Description: Class that implementschange return calculator class.
 * 
 */

using System;

namespace TrainingApplication
{
    class ChangeReturn
    {
        public static void Change()
        {
            var loop = true;

            int fiveHundred, twoHundred, hundred, fifty, twenty, ten, five, two, one, fiftyCent, twentyCent, tenCent, fiveCent, twoCent, cent;

            Console.WriteLine("\n*** Change return calculator ***");

            while (loop)
            {
                Console.Write("\nEnter cost: ");
                string costInput = Console.ReadLine().ToLower();
                if (costInput == "0" || costInput == "exit" || costInput == "back" || costInput == "quit")
                {
                    loop = false;
                }
                else if (costInput == "help" || costInput == "info")
                {
                    // TODO Read help file.
                    Console.WriteLine("Coming soon!");
                }
                else
                {
                    Console.Write("Enter amount of given money: ");
                    if (double.TryParse(Console.ReadLine(), out double amount) && double.TryParse(costInput, out double cost))
                    {
                        double amountOfChange = amount - cost;

                        if (amountOfChange < 0)
                        {
                            Console.WriteLine("\nNot enough money!");
                        }
                        else if (amountOfChange == 0)
                        {
                            Console.WriteLine("\nNo need for change.");
                        }
                        else
                        {
                            (amountOfChange, fiveHundred) = CalculateReturn(amountOfChange, 500);
                            (amountOfChange, twoHundred) = CalculateReturn(amountOfChange, 200);
                            (amountOfChange, hundred) = CalculateReturn(amountOfChange, 100);
                            (amountOfChange, fifty) = CalculateReturn(amountOfChange, 50);
                            (amountOfChange, twenty) = CalculateReturn(amountOfChange, 20);
                            (amountOfChange, ten) = CalculateReturn(amountOfChange, 10);
                            (amountOfChange, five) = CalculateReturn(amountOfChange, 5);
                            (amountOfChange, two) = CalculateReturn(amountOfChange, 2);
                            (amountOfChange, one) = CalculateReturn(amountOfChange, 1);
                            (amountOfChange, fiftyCent) = CalculateReturn(amountOfChange, 0.50);
                            (amountOfChange, twentyCent) = CalculateReturn(amountOfChange, 0.20);
                            (amountOfChange, tenCent) = CalculateReturn(amountOfChange, 0.10);
                            (amountOfChange, fiveCent) = CalculateReturn(amountOfChange, 0.05);
                            (amountOfChange, twoCent) = CalculateReturn(amountOfChange, 0.02);
                            (amountOfChange, cent) = CalculateReturn(amountOfChange, 0.01);

                            Console.WriteLine("\nChange: ");
                            PrintReturn(fiveHundred, "500 eur");
                            PrintReturn(twoHundred, "200 eur");
                            PrintReturn(hundred, "100 eur");
                            PrintReturn(fifty, "50 eur");
                            PrintReturn(twenty, "20 eur");
                            PrintReturn(ten, "10 eur");
                            PrintReturn(five, "5 eur");
                            PrintReturn(two, "2 eur");
                            PrintReturn(one, "1 eur");
                            PrintReturn(fiftyCent, "50 cen");
                            PrintReturn(twentyCent, "20 cent");
                            PrintReturn(tenCent, "10 cent");
                            PrintReturn(fiveCent, "5 cent");
                            PrintReturn(twoCent, "2 cent");
                            PrintReturn(cent, "1 cent");
                            Console.WriteLine();
                        }
                    }
                    else
                    {
                        Console.WriteLine("Invalid input!");
                    }
                }
            }
        }

        private static (double, int) CalculateReturn(double change, double number)
        {
            int amount = 0;

            while (change - number >= 0.00)
            {
                change -= number;
                amount++;
            }

            return (change, amount);
        }

        private static void PrintReturn(int amount, string unit)
        {
            if (amount > 0)
            {
                Console.WriteLine($"  {amount} ({unit})");
            }
        }
    }
}
