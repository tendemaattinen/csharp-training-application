﻿/*
 * Author: Teemu Tynkkynen
 * Description: Class which mask passwords characters with '*' whenb typing password.
 * 
 */

using System;

namespace TrainingApplication
{
    class MaskPassword
    {
        // Method that masks password when typing it. Return password as string.
        public static string Mask()
        {
            var password = "";
            do
            {
                ConsoleKeyInfo key = Console.ReadKey(true);
                // Adds pressed key to string if it is not Backspace or Enter.
                if (key.Key != ConsoleKey.Backspace && key.Key != ConsoleKey.Enter)
                {
                    password += key.KeyChar;
                    Console.Write("*");
                }
                else
                {
                    // Deletes char from password when pressed Backspace.
                    if (key.Key == ConsoleKey.Backspace && password.Length > 0)
                    {
                        password = password.Substring(0, (password.Length - 1));
                        Console.Write("\b \b");
                    }
                    // Breaks loop when Enter is pressed.
                    else if (key.Key == ConsoleKey.Enter)
                    {
                        break;
                    }
                }
            } while (true);

            return password;
        }
    }
}
