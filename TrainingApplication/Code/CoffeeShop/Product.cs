﻿/*
 * Author: Teemu Tynkkynen
 * Description: 
 * 
 */

using System;
using System.Collections.Generic;
using System.Text;

namespace TrainingApplication.CoffeeShop
{
    
    /// <summary>
    /// Product class.
    /// </summary>
    class Product
    {
        protected double cost;
        protected string description;

        /// <summary>
        /// 
        /// </summary>
        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }

        /// <summary>
        /// Sets cost of product and returns cost of product.
        /// </summary>
        public double Cost
        {
            get
            {
                return cost;
            }
            set
            {
                cost = value;
            }
        }
    }
    /// <summary>
    /// Sweet class, derived from product.
    /// </summary>
    class Sweet : Product
    {
        
    }

    /// <summary>
    /// Bun class, derived from sweet class
    /// </summary>
    class Bun : Sweet
    {
        public Bun()
        {
            cost = 1.49;
            description = "Sweet bun";
        }
    }

    /// <summary>
    /// Beverage class, derived from product class.
    /// </summary>
    class Beverage : Product
    {
        private void Make()
        {

        }
    }

    /// <summary>
    /// Coffee class, derived from beverage class.
    /// </summary>
    class Coffee : Beverage
    {
        private bool milk;
        private bool cream;
        private bool sugar;

        /// <summary>
        /// 
        /// </summary>
        public Coffee()
        {
            cost = 0.49;
            description = "Coffee";
            milk = Milk();
            if (!milk)
            {
                cream = Cream();
            }
            sugar = Sugar();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool Milk()
        {
            Console.Write("Milk (y/n): ");
            if (Console.ReadLine().ToLower() == "y")
            {
                cost += 0.10;
                description += " with milk";
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool Cream()
        {
            Console.Write("Cream (y/n): ");
            if (Console.ReadLine().ToLower() == "y")
            {
                cost += 0.25;
                description += " with cream";
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool Sugar()
        {
            Console.Write("Sugar (y/n): ");
            if (Console.ReadLine().ToLower() == "y")
            {
                cost += 0.15;
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    class Placeholder : Product
    {

    }

}
