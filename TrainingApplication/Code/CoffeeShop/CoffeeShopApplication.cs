﻿/*
 * Author: Teemu Tynkkynen
 * Description: Ordering application for coffee shop.
 * 
 */

using System;
using System.Collections.Generic;
using System.IO;

namespace TrainingApplication.CoffeeShop
{
    /// <summary>
    /// Contains basic methods for coffee shop application.
    /// </summary>
    class CoffeeShopApplication
    {
        /// <summary>
        /// Main method of coffee shop application.
        /// </summary>
        public void Main()
        {
            var loop = true;

            while (loop)
            {
                switch(MainMenu())
                {
                    case "1":
                        Order();
                        break;
                    case "2":
                        Catalog();
                        break;
                    case "0000":
                        EditMenu();
                        
                        break;
                    case "info":
                    case "help":

                        break;
                    case "0":
                    case "exit":
                    case "back":
                        loop = false;
                        Console.WriteLine("Thanks for choosing our coffee shop!");
                        break;
                    default:
                        Console.WriteLine("Invalid input!");
                        break;
                }
            }
        }

        /// <summary>
        /// Prints main menu and asks user input.
        /// </summary>
        /// <returns>
        /// User input as a string.
        /// </returns>
        private string MainMenu()
        {
            Console.WriteLine();
            Console.WriteLine("*** Coffee Shop ***");
            Console.WriteLine();
            Console.WriteLine($"Good {TimeOfTheDay(GetTime())}! How can i serve you today?");
            Console.WriteLine();
            Console.WriteLine("1) New order");
            Console.WriteLine("2) Catalog");
            Console.WriteLine("0) Exit");
            Console.Write("Enter: ");
            return Console.ReadLine().ToLower();
        }

        // Printing catalog.
        private void Catalog()
        {

        }

        /// <summary>
        /// Secret edit menu.
        /// </summary>
        private void EditMenu()
        {
            var loop = true;
            Console.WriteLine("This is top secret menu!");
            while (loop)
            {
                Console.WriteLine();
                Console.WriteLine("1) Reset receipt counter");

                Console.Write("Enter: ");
                switch (Console.ReadLine().ToLower())
                {
                    case "1":
                        var counterNumber = ReadSetting("receipt counter");
                        Console.WriteLine($"Counter now: {counterNumber}");
                        Console.Write("Reset (y/n): ");
                        if (Console.ReadLine().ToLower() == "y")
                        {
                            // ResetReceiptCounter();
                        }
                        else
                        {
                            Console.WriteLine("Rsseting canceled!");
                        }
                        break;
                    case "back":
                    case "exit":
                    case "0":
                        loop = false;
                        break;
                    default:
                        Console.WriteLine("Invalid input!");
                        break;
                }
            }
        }

        // Method to reset receipt counter in the setting text file. Useless now.
        private void ResetReceiptCounter()
        {
            var settingName = "receipt counter";
            try
            {
                using (StreamReader sr = new StreamReader("CoffeeShop/settings.txt"))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        if (line.Contains(settingName))
                        {
                            // line.Replace();
                            break;
                        }
                        Console.WriteLine(line);

                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// Gets time now.
        /// </summary>
        /// <returns>
        /// Time now as DateTime.
        /// </returns>
        private DateTime GetTime()
        {
            DateTime timeNow = DateTime.Now;
            return timeNow;
        }

        // Method for getting time of the day.
        /// <summary>
        /// Gets name for time of the day.
        /// </summary>
        /// <param name="time">Datetime</param>
        /// <returns>
        /// Name of the time of the day as string.
        /// </returns>
        private string TimeOfTheDay(DateTime time)
        {
            string timeOfTheDay;
            if ((time.Hour >= 5) && (time.Hour < 11))
            {
                timeOfTheDay = "morning";
            }
            else if ((time.Hour >= 12) && (time.Hour < 17))
            {
                timeOfTheDay = "afternoon";
            }
            else if ((time.Hour >= 17) && (time.Hour < 22))
            {
                timeOfTheDay = "evening";
            }
            else
            {
                timeOfTheDay = "night";
            }

            return timeOfTheDay;
        }

        /// <summary>
        /// Make a order.
        /// </summary>
        private void Order()
        {
            var factory = new ProductFactory();
            Product product;
            var loop = true;

            // Shopping basket for ordered products.
            var basket = new List<Product>() { };

            while (loop)
            {
                Console.WriteLine();
                Console.Write("Enter product number, 'catalog' or '0' to quit: ");
                var input = Console.ReadLine().ToLower();
                // Check if user wants to see a catalog.
                if (input == "catalog")
                {
                    Catalog();
                }
                // Check if user wants to go back.
                else if (input == "back" || input == "quit" || input == "0")
                {
                    loop = false;
                }
                else if (input == "info" || input == "help")
                {
                    // Info here!

                }
                // Check if input is a number.
                else if (int.TryParse(input, out int productNumber))
                {
                    product = factory.CreateProduct(productNumber);
                    Console.Write($"Add {product.Description} to basket (y/n): ");
                    switch (Console.ReadLine().ToLower())
                    {
                        case "y":
                            basket.Add(product);
                            break;
                        default:
                            Console.WriteLine();
                            Console.WriteLine($"{product.Description} not added!");
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("Invalid input!");
                }
            }

            CreateReceipt(basket);
        }

        private void CreateReceipt2()
        {

        }

        /// <summary>
        /// Method to read specific setting from settings file. Useless right now, maybe convert to xlm, csv or json
        /// </summary>
        /// <param name="settingName">Settings name as string.</param>
        /// <returns>Nothing right now as string.</returns>
        private string ReadSetting(string settingName)
        {
            var setting = "null";

            try
            {
                using (StreamReader sr = new StreamReader("CoffeeShop/settings.txt"))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        if (line.Contains(settingName))
                        {
                            var arr = line.Split("=");
                            setting = arr[1];
                            break;
                        }
                        Console.WriteLine(line);
                        
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return setting;
        }

        /// <summary>
        /// Create a receipt.
        /// </summary>
        /// <param name="list">List of Products</param>
        private void CreateReceipt(List<Product> list)
        {
            var totalCost = 0.00;
            var time = GetTime().ToString();
            var time2 = time;

            var charsToRemove = new string[] { "@", ",", ".", ";", "'", " " };
            foreach (var c in charsToRemove)
            {
                time2 = time2.Replace(c, string.Empty);
            }

            var fileName = "CoffeeShop/Receipts/" + time2 + ".txt";

            using (StreamWriter sr = File.CreateText(fileName))
            {
                sr.WriteLine("         *** COFFEE SHOP ***");
                sr.WriteLine();
                sr.WriteLine($"TIME: {time}");
                sr.WriteLine();
                foreach (Product pro in list)
                {
                    sr.WriteLine(String.Format("{0,-30} {1,5}", pro.Description, pro.Cost));
                    totalCost += pro.Cost;
                }

                sr.WriteLine();
                sr.WriteLine(String.Format("{0,-30} {1,5}", "Total cost:", totalCost));
            }
            

            // Print version of the receipt.
            Console.WriteLine();
            Console.WriteLine("         *** COFFEE SHOP ***");
            Console.WriteLine();
            Console.WriteLine($"TIME: {time}");
            Console.WriteLine();
            foreach (Product pro in list)
            {
                Console.WriteLine(String.Format("{0,-30} {1,5}", pro.Description, pro.Cost));
            }

            Console.WriteLine();
            Console.WriteLine(String.Format("{0,-30} {1,5}", "Total cost:", totalCost));
        }
    }

    /// <summary>
    /// Factory class which produce products.
    /// </summary>
    class ProductFactory
    {
        /// <summary>
        /// Main method of the class.
        /// </summary>
        /// <param name="productId">Production id as integer.</param>
        /// <returns>Product as Product</returns>
        public Product CreateProduct(int productId)
        {
            Product product;

            switch (productId)
            {
                case 1:
                    product = new Coffee();
                    break;
                case 2:
                    product = new Bun();
                    break;
                default:
                    product = new Placeholder();
                    break;
            }

            return product;
        }
    }
}