﻿/*
 * Author: Teemu Tynkkynen
 * Description: Main program after login in.
 * 
 */

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data.SQLite;

namespace TrainingApplication
{
    class MainProgram
    {
        public void Main(User user, SQLiteConnection connection)
        {
            bool loop = true;
            var isAdmin = user.IsAdmin;
            Console.WriteLine();
            Console.Write("*** My training application ***");
            Console.WriteLine();
            while (loop)
            {
                loop = ChooseAction(PrintMainMenu(isAdmin), isAdmin, connection);
            }
        }

        private string PrintMainMenu(bool admin)
        {
            Console.WriteLine();
            Console.WriteLine("1) Applications");
            Console.WriteLine("2) Account settings (WIP)");
            Console.WriteLine("3) Info");
            if (admin)
            {
                Console.WriteLine("4) Admin menu");
            }
            Console.WriteLine("0) Log out");
            Console.Write("Enter: ");
            return Console.ReadLine().ToLower();
        }

        private bool ChooseAction(string input, bool admin, SQLiteConnection conn)
        {
            var continueLoop = true;
            switch (input)
            {
                case "1":
                    Applications.ApplicationsMenu();
                    break;
                case "2":
                    // TODO Open account settings page.

                    break;
                case "3":
                case "help":
                case "info":
                    TextFileReader.ReadTextFile("infoHelpStart", "infoHelpEnd");
                    break;
                case "4":
                    if (admin)
                    {
                        AdminMenu(conn);
                    }
                    else
                    {
                        Console.WriteLine("Invalid input!");
                    }
                    break;
                case "0":
                case "quit":
                case "q":
                case "exit":
                    continueLoop = false;
                    Console.WriteLine("Logged out!");
                    break;
                default:
                    Console.WriteLine("Invalid input!");
                    break;
            }
            return continueLoop;
        }

        private bool AdminMenu(SQLiteConnection conn)
        {
            var continueLoop = true;
            Console.WriteLine();
            Console.WriteLine("1) Show all users in database");
            Console.WriteLine("2) Add user to database TODO");
            Console.WriteLine("3) Delete user from database TODO");
            Console.WriteLine("0) Back");
            Console.Write("Enter: ");

            switch (Console.ReadLine().ToLower())
            {
                case "1":
                    ShowAllUsers(conn);
                    break;
                case "2":

                    break;
                case "0":
                    continueLoop = false;
                    break;
            }
            return continueLoop;
        }

        private void ShowAllUsers(SQLiteConnection conn)
        {
            Console.WriteLine();

            var sql = "SELECT uid, username FROM accounts";
            SQLiteCommand command = new SQLiteCommand(sql, conn);
            SQLiteDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                Console.WriteLine($"{reader.GetInt32(0)}) {reader.GetString(1)}");
            }

            reader.Close();
        }
    }
}
