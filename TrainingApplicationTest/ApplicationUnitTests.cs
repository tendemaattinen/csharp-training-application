using System;
using TrainingApplication;
using Xunit;

namespace TrainingApplicationTest
{
    public class CurrencyConverterTests
    {
        [Theory]
        [InlineData (4, 0.8, 0.7, 3.5)]
        [InlineData (100, 0.5, 0.2, 40)]
        public void CalculateValue_SingleValue_ReturnsCorrectAnswer(double value, double factor1, double factor2, double answer)
        {
            Assert.Equal(answer, CurrencyConverter.CalculateValue(value, factor1, factor2));
        }
    }

    public class FibonacciSequenceTests
    {
        [Theory]
        [InlineData(5L, 5UL)]
        [InlineData(30L, 832040UL)]
        public void CalculateNthNumber_SingleNumber_ReturnsCorrectAnswer(long number, ulong answer)
        {
            Assert.Equal(answer, FibonacciSequence.CalculateNthNumber(number));
        }
    }

    public class CreditCardValidatorTests
    {
        [Theory]
        [InlineData ("5555555555554444", "4111111111111111", "5552555585554444", "555255558555444636643643", "55525555855544")]
        public void LuhnAlgorithm_CreditCardNumber_RetursTrueTest(string realCard1, string realCard2, string fakeCard, string tooLongCard, string tooShortCard)
        {
            Assert.True(CreditCardValidator.LuhnAlgorithm(realCard1));
            Assert.True(CreditCardValidator.LuhnAlgorithm(realCard2));
            Assert.False(CreditCardValidator.LuhnAlgorithm(fakeCard));
            Assert.False(CreditCardValidator.LuhnAlgorithm(tooLongCard));
            Assert.False(CreditCardValidator.LuhnAlgorithm(tooShortCard));
        }
    }
}
